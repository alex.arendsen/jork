const jork = require('../public/javascripts/jork.js');
var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { jork: jork.getJorkd(), title: 'JORK' });
});

router.get('/json', function(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.send(jork.getJorkd());
});

module.exports = router;
