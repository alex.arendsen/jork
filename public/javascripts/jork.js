const prefixes = ['jor', 'geor', 'chor']
const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('')
const vowels = 'aeiou'.split('')
const consonants = [...alphabet.filter(c => vowels.indexOf(c) === -1), 'ch', 'sh']
const smooshes = ['', 'an', 'in', 'en', 'y', 'ie']
const suffixes = ['stein', 'stern', 'town', 'berger', 'man', 'son', ' the eighth']
const olikevowels = ['o', 'u', 'ou', 'oe', 'ue']

function clampedRandom(min, max) {
  return Math.round((Math.random() * (max - min)) + min)
}

function chooseRandom(options) {
  return options[clampedRandom(0, options.length - 1)]
}

function capitalizationParty(src) {
  const specialSauce = clampedRandom(0, 1);

  switch(clampedRandom(0, 3)) {
    case 0: return src.toUpperCase(); break
    case 1: return src.toLowerCase(); break
    case 2: return src.split('').reduce((out, next) => out + ((out.length % 2 === specialSauce) ? next.toUpperCase() : next ), ''); break
    case 3: return src
  }
}

function getJorkd() {
  let prefix = chooseRandom(prefixes)
  if (0 === clampedRandom(0, 4)) prefix = prefix.replace('o', chooseRandom(olikevowels))

  const consonant = chooseRandom(consonants)
  const smoosh = chooseRandom(smooshes)
  const suffix = clampedRandom(0, 4) === 0 ? chooseRandom(suffixes) : '';

  return capitalizationParty(`${prefix}${consonant}${smoosh}${suffix}`)
}

if (typeof window === 'undefined')
  module.exports = { getJorkd }
